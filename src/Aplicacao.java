package edu.cest.lab01;

public class Aplicacao {
	public static void main(String[] args) {
		System.out.println("\n\nInicio do teste das classes Pessoa, Cliente e Animal.");

		System.out.println("\nTestando Classe Pessoa.");
		Pessoa pessoateste = new Pessoa();
		pessoateste.nome = "Nome Pessoa";
		System.out.print("Nome da Pessoa: ");
		System.out.println(pessoateste.nome);
		System.out.println("Fim do Teste da Classe Pessoa.");

		System.out.println("\nTestando Classe Cliente.");
                Cliente clienteteste = new Cliente();
                clienteteste.nome = "Nome Cliente";
                System.out.print("Nome do Cliente: ");
		System.out.println(clienteteste.nome);
                System.out.println("Fim do Teste da Classe Clinte.");
   
		System.out.println("\nTestando Classe Animal.");
                Animal animalteste = new Animal();
                animalteste.nome = "Nome Animal";
                System.out.print("Nome do Animal: ");
		System.out.println(animalteste.nome);
                System.out.println("Fim do Teste da Clasee Animal.");
		
		System.out.println("\nFim do teste das classes Pessoa, Cliente e Animal.\n\n");


	}
}

